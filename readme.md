**1. Run Terrform to create server EC2**

    - terrform init
    
    - terrform plan
    
    - terraform apply

  

**2. Run Ansible to build image**

    

    - ansible-playbook build_image.yml

  

**3. Run Ansible to deploy to server**

    

    - ansible-playbook deploy_container.yml

  

**4. Test API of app**

    - POST /files - Upload file
    
    - DELETE /files/delete/yourfile.ext - Delete file
    
    - GEt /files/list - list file uploaded
   
**5. Cost**

    - t2.micro - $0.007/hour
