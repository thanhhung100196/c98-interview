# Start from the official Go base image
FROM golang:1.20-alpine

# Set the working directory inside the container
WORKDIR /app

# Copy the Go application source code to the container
COPY files_storage.go files_storage.go

# Build the Go 
RUN go mod init files_storage.go
RUN go build -o main

# Set the entry point for the container
ENTRYPOINT ["./main"]