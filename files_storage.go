package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const (
	uploadDirectory = "uploads" // Directory to store uploaded files
)

func main() {
	initialize()

	http.HandleFunc("/files", uploadFileHandler)
	http.HandleFunc("/files/", getFileHandler)
	http.HandleFunc("/files/delete/", deleteFileHandler)
	http.HandleFunc("/files/list", listFilesHandler)

	fmt.Println("Server is running on http://localhost:5000")
	http.ListenAndServe(":5000", nil)
}

func initialize() {
	if _, err := os.Stat(uploadDirectory); os.IsNotExist(err) {
		os.Mkdir(uploadDirectory, os.ModePerm)
	}
}

func uploadFileHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(10 << 20) // Maximum file size is set to 10MB

	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, "No file provided", http.StatusBadRequest)
		return
	}
	defer file.Close()

	filePath := filepath.Join(uploadDirectory, handler.Filename)
	if _, err := os.Stat(filePath); !os.IsNotExist(err) {
		http.Error(w, "File already exists", http.StatusConflict)
		return
	}

	out, err := os.Create(filePath)
	if err != nil {
		http.Error(w, "Error creating file", http.StatusInternalServerError)
		return
	}
	defer out.Close()

	_, err = io.Copy(out, file)
	if err != nil {
		http.Error(w, "Error writing file data", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "File uploaded successfully")
}

func getFileHandler(w http.ResponseWriter, r *http.Request) {
	filename := filepath.Base(r.URL.Path)

	filePath := filepath.Join(uploadDirectory, filename)
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		http.Error(w, "File not found", http.StatusNotFound)
		return
	}

	http.ServeFile(w, r, filePath)
}

func deleteFileHandler(w http.ResponseWriter, r *http.Request) {
	filename := filepath.Base(r.URL.Path)

	filePath := filepath.Join(uploadDirectory, filename)
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		http.Error(w, "File not found", http.StatusNotFound)
		return
	}

	err := os.Remove(filePath)
	if err != nil {
		http.Error(w, "Error deleting file", http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "File deleted successfully")
}

func listFilesHandler(w http.ResponseWriter, r *http.Request) {
	files, err := os.ReadDir(uploadDirectory)
	if err != nil {
		http.Error(w, "Error reading upload directory", http.StatusInternalServerError)
		return
	}

	var fileNames []string
	for _, file := range files {
		fileNames = append(fileNames, file.Name())
	}

	response := strings.Join(fileNames, "\n")
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprint(w, response)
}
