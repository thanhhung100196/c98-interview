provider "aws" {
  region     = "us-east-2" # replace with your region
}

resource "aws_instance" "files_storage" {
  ami           = "ami-00399ec92321828f5" # Ubuntu 20.04
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.allow_port.name}"]
  subnet_id = subnet-96ef51ed # replace with your subnet
  user_data              = <<-EOF
                            #!/bin/bash
                            sudo apt-get update
                            sudo apt-get install -y docker.io
                            sudo usermod -aG docker ubuntu
                            #Install Ansible
                            sudo apt install software-properties-common
                            sudo add-apt-repository --yes --update ppa:ansible/ansible
                            sudo apt install ansible
                            EOF
}

resource "aws_security_group" "allow_port" {
  name        = "files-storage-security-group"
  description = "files storage security group"

  # Other configuration options for your security group
  # ...
}

resource "aws_security_group_rule" "allow_port_ingress_rule_1" {
  security_group_id = aws_security_group.allow_port.id
  type              = "ingress"
  from_port         = 5000
  to_port           = 5000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]  # Replace with your desired source IP range
}

resource "aws_security_group_rule" "allow_port_ingress_rule_2" {
  security_group_id = aws_security_group.allow_port.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]  # Replace with your desired source IP range
}
